<?php
// This Includes the database Configuration Files that setup database
include 'dbConfig.php';
$statusMsg = '';

// File upload path
$targetDir = "uploads/";
$fileName = basename($_FILES["file"]["name"]);
$targetFilePath = $targetDir . $fileName;
$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

if(isset($_POST["submit"]) && !empty($_FILES["file"]["name"])){
    // Allow Following Image Formats
    $allowTypes = array('jpg','png','jpeg','gif','pdf','bmp');
    if(in_array($fileType, $allowTypes)){
        // Upload file to server
        if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
            // Insert image file name into SQL database
            $insert = $db->query("INSERT into images (file_name, uploaded_date) VALUES ('".$fileName."', NOW())");
            if($insert){
                $statusMsg = "The file ".$fileName. " has been uploaded successfully.";
                
            }else{
                $statusMsg = "File upload failed, please try again.";                
            } 
        }else{
            $statusMsg = "Sorry, there was an error uploading your file.";
        }
    }else{
        $statusMsg = 'Sorry, only JPG, JPEG, PNG, GIF, BMP & PDF files are allowed to upload.';
    }
}else{
    $statusMsg = 'Please select a file to upload.';
}


if (strpos($statusMsg, 'has been uploaded successfully.') !== false) 
{
//Successful Upload
echo '<script> window.location = "page_b.php" </script>'; 


}


if (strpos($statusMsg, 'has been uploaded successfully.') !== true) 
{
//Unsuccessful Upload

echo '<script> window.location = "homepage_error.html" </script>'; 
}


?>