<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>popup_b</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>

    <style>
           .box{
        	width:385px;
        	height:90px;
            }
            
            .blue{
	        background: #044b8d;
            }   

            img{            
            width: 90px;
            height: 90px;
            background:  #044b8d;
            }

            .modal-content {
                float: left;
            }
            
            .text-content {
                display: table-cell;
            }

            html, body {
                font-family: 'Open Sans', Arial, Helvetica, sans-serif;
                font-size: 25px;
                margin: 0;
                min-height: 180px;
                width: 384px;
                
            }
      
            .modal-icons {
                border-top: 35px solid  #044b8d;
                height: 50px;
                width: 100%;
            }
             
            .version{
                color: #444;
                font-size: 18px;
            }
            
            button.link_one{ 
                background:none;
                border:none; 
                font-size: 15px;
                text-align:center;
            }

            button.link_one:hover{ 
             color: #6085ec;
            }
            </style>
            
            <div class="box blue"> 
                    <div class="modal-header"
                    <h1 class="logo">
                     <img src="images/privateeye(125).png" alt="EyeSpy" class="logo-icon"> 
                    
                    </h1>
                </div>

            </div>
            
            </head>
            <body>
                       
            <div class="modal-content">
                    <img src=images/sample.png alt="sample" align="left">
                    <br>
                    <br>
  
            </div>

            <div class="text-content">      
                    
                    <br>
                    <br>
                    <textarea rows="" cols="25" id="textbox">Type Hidden Message Here...</textarea>
                    <br>
                    <br>
                    <button onclick="clicked_three()" class="link_one" id="buttonOne">Hide Data</button>
                        </div>

            <div class="modal-icons">
               
               </div>
            
               <script src="script_routing.js"></script>
            </body>
            </html>