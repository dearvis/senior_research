<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> First Program with Xampp Server </title>
    <style media="screen">
  
</style>

</head>
<body>
<form action="upload.php" method="POST" enctype="multipart/form-data" >
    <div class="specialcontainer">
    <input type="file" name="file" id="file" class="inputfile">
    </div>
    <div class="inner"></div>
    <button type="submit" name="submit" class="uploadbuttonstyle">Upload</button>
    </div>
</form>

<div class="gallery">
    <h1>Image Uploaded</h1>
    <?php
    // Include the database configuration file
    include 'dbConfig.php';
    
    // Get images from the database
    $query = $db->query("SELECT * FROM images ORDER BY uploaded_date DESC");
    
    if($query->num_rows > 0){
        while($row = $query->fetch_assoc()){
            $imageURL = 'uploads/'.$row["file_name"];
    ?>
        <img src="<?php echo $imageURL; ?>" alt="" />
    <?php }
    }else{ ?>
        <p>No image(s) found...</p>
    <?php } ?>
</div>

</body>
</html>