const input  = document.getElementById('image');
const canvas = document.getElementById('uploaded_img');
const context = canvas.getContext('2d');
const canvas_two = document.getElementById('myCanvas');
const ctx = canvas_two.getContext('2d');

var imageData = ""
var encryptedMessage = "";
var bits = 0;
var tracker = 0;
var arr = [];
var empty_bits = false;

input.addEventListener('change',function (e)
 {
    const reader = new FileReader();
    reader.onload = function () 
    {
        const img = new Image()
        img.onload = function() 
        {    
        
        // Resizes the canvas to match that of the image
        var max_size = 170;
        var width = img.width;
        var height = img.height;

        if (width > height)
        {
            if (width > max_size) 
            {
                height *= max_size / width;
                width = max_size;
            }
        }
        else
        {
            if (height > max_size)
            {
                width *= max_size / height;
                height = max_size;
            }
        }    

        canvas.width = width;
        canvas.height = height;
        context.drawImage(img, 0, 0, width, height);
        imageData = context.getImageData(0,0,canvas.width,canvas.height);

        const pixel_data = imageData.data;

        for (var i = 0; i <= pixel_data.length - 1; i += 4)
        {
             //Pushes the Pixels last two bits in "arr" array
             decrypt(pixel_data[i]);
             
             decrypt(pixel_data[i + 1]);
            
             decrypt(pixel_data[i + 2]);
        }
        context.putImageData(imageData, 0, 0);   
        loadImg();         
    }
        img.src = reader.result;
}
        reader.readAsDataURL(input.files[0]); //File reader reads uploaded file from input
});







function decimalToBinary(val)  //Convert pixel value to 8-Bit Binary (function)
{
    if(val != undefined)
    {
        var output = ("00000000"+val.toString(2)).slice(-8);
        return output;
    }
    return 0; //Last Pixel gives undefined value, This identifies last pixel
}



function decrypt(pixel_RGB_value) //Pushes the Pixels last two bits in "arr" array
{
    var complete_bin = decimalToBinary(pixel_RGB_value);        // Gets Binary value of selected Pixel

    var last_two_bits = complete_bin.toString(2).slice(-2);     // Select the last two bits in the Pixel

    if(pixel_RGB_value != undefined)
    {
        var bin = (last_two_bits+"000000").slice(0,8);
        arr.push(bin);                                   //Push the last two bits of pixel values in "arr" array
    }
}

function loadImg()
{
     const readerTwo = new FileReader();    // Load canvas that will display Secret image 
 
     //Get Image data that was uploaded
     readerTwo.onload = function () 
     {
         const imgTwo = new Image();
 
         imgTwo.onload = function() 
         {
             var max_size = 170;// TODO : pull max size from a site config
             var width = imgTwo.width;
             var height = imgTwo.height;
  
              if (width > height)
             {
                 if (width > max_size) 
                 {
                     height *= max_size / width;
                     width = max_size;
                 }
             }
             else
             {
                 if (height > max_size)
                 {
                     width *= max_size / height;
                     height = max_size;
                 }
             }
             
             canvas_two.width = width;
             canvas_two.height = height;
 
             const pixel_data = imageData.data;
     
             for (var i = 0; i <= pixel_data.length - 1; i += 4)
              {             
               
               var alt_r = new_pixel(pixel_data[i], i);       
               var alt_g = new_pixel(pixel_data[i+1], i);
               var alt_b = new_pixel(pixel_data[i+2], i);


               pixel_data[i] = bin_to_dec(alt_r);       
               pixel_data[i + 1] =  bin_to_dec(alt_g);
               pixel_data[i + 2] = bin_to_dec(alt_b);

         //      console.log("Hidden Data arr Values (RGB): " + bin_to_dec(arr[i]) + " , " + bin_to_dec(arr[i+1]) + " , " +  bin_to_dec(arr[i+2]) );
             }
 
             altered_imageData = ctx.getImageData(0,0,canvas_two.width,canvas_two.height);
             imgTwo.style.display = 'none';
             ctx.putImageData(imageData, 0, 0);
         } 
             
         imgTwo.src = readerTwo.result;
     }
     
         readerTwo.readAsDataURL(input.files[0]);
 }

 function bin_to_dec(string)
 { 
    return parseInt((string + '').replace(/[^01]/gi, ''), 2);
}





function new_pixel(val, arr_pos)  //Convert pixel value to 8-Bit Binary (function)
 {
   var zeros = "000000"; 
   var new_val = "";
   var hidden_img_msb = arr[arr_pos];


    if(val != undefined)
     {
        
        var output = ("00000000"+val.toString(2)).slice(-8);
        new_val = hidden_img_msb;
        output = new_val + zeros;

        console.log("Altered Pixel Value is: " + output);
        console.log("");
        return output;
     }
     
     return "00000000"; //Last Pixel gives undefined value, This identifies last pixel
 }