var optionsPage_buttonOne = document.getElementById('buttonOne');
var optionsPage_buttonTwo = document.getElementById('buttonTwo');
var optionsPage_buttonThree = document.getElementById('buttonThree');
var optionsPage_buttonFour = document.getElementById('buttonFour');


//Options Page
if( optionsPage_buttonOne != undefined)  //Encrypt Message in Image
{
    optionsPage_buttonOne.addEventListener("click", redirect_messageEncrypt, false);
}
 
if( optionsPage_buttonTwo != undefined)    //Encrypt Image inside of Image
{
    optionsPage_buttonTwo.addEventListener("click", redirect_imageEncrypt, false);
}
 
if( optionsPage_buttonThree != undefined)    //Decrypt Message inside of Image
{
    optionsPage_buttonThree.addEventListener("click", redirect_messageDecrypt, false);
}
 
if( optionsPage_buttonFour != undefined)    //Decrypt image inside of Image
{
    optionsPage_buttonFour.addEventListener("click", redirect_imageDecrypt, false);
}
 



function redirect_messageEncrypt(e) 
{
    location.href = "messageEncrypt.html";
}

function redirect_imageEncrypt(e) 
{
    location.href = "imageEncrypt.html";
}

function redirect_messageDecrypt(e) 
{
    location.href = "messageDecrypt.html";
}

function redirect_imageDecrypt(e) 
{
    location.href = "imageDecrypt.html";
}



