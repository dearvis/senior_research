var coverImage_upload = document.getElementById('coverImageUpload');
var hiddenImage_upload = document.getElementById('hiddenImageUpload');

var coverImage_caption = document.getElementById('caption1');
var hiddenImage_caption = document.getElementById('caption2');

var canvas = document.getElementById('coverImage');
var canvas_two = document.getElementById('hiddenImage');
var canvas_three = document.getElementById('New_Image');
var canvas_cube = document.getElementById('color_cube');

var context = canvas.getContext('2d');
var ctx = canvas_two.getContext('2d');
var canv_three_ctx = canvas_three.getContext('2d');
var ctx_cube = canvas_cube.getContext("2d");
var imageData = "";

var cover_img_input = [];
var hidden_img_pix = [];

var cover_img_arr = [];
var new_img_arr = [];

var arrayPos = 0;

var final_img_map = new Map();
var cover_img_map = new Map();
var hidden_img_map = new Map();

//Button Functionality
var hide_Bttn = document.getElementById('hide_Image_Bttn');
hide_Bttn.addEventListener("click", encrypt_image, false);









coverImage_upload.addEventListener('change',function (e) //Displays Uploaded Img
 {
    // Canvas
    const reader = new FileReader();
    reader.onload = function () 

    {
        const img = new Image()
        img.onload = function() 
        {    
           // Resizes the canvas to match that of the image
            var max_size = 170;
            var width = img.width;
            var height = img.height;

            if (width > height)
            {
                if (width > max_size) 
                {
                    height *= max_size / width;
                    width = max_size;
                }
            }
            else
            {
                if (height > max_size)
                {
                    width *= max_size / height;
                    height = max_size;
                }
            }    

            canvas.width = width;
            canvas.height = height;
            context.drawImage(img, 0, 0, width, height);

            // console.log("Image 1 Max Size: " + max_size + " Image 1 Width: " + width + " Image 2 height: " + height);
            imageData = context.getImageData(0,0,canvas.width,canvas.height);
            const pixel_data = imageData.data;
        
            for (var i = 0; i <= pixel_data.length; i += 4)
            {
               //Convert RGB values from img to 8-bit Binary  //
               store_coverImg(pixel_data[i], i);
               store_coverImg(pixel_data[i + 1], (i+1));
               store_coverImg(pixel_data[i + 2], (i+2));
            }            
        
            imageData = context.getImageData(0,0,canvas.width,canvas.height);
            context.putImageData(imageData, 0, 0);
    }
             img.src = reader.result;
}
            reader.readAsDataURL(coverImage_upload.files[0]); //File reader reads uploaded file from input
            cover_img_input.push(coverImage_upload.files[0]); //Push uploaded cover image data in array

});


hiddenImage_upload.addEventListener('change',function (e)
 {
    const reader_two = new FileReader();
    reader_two.onload = function () 
    {
        const img_two = new Image()
        img_two.onload = function() 
        {    
           // Resizes the canvas to match that of the image
            var max_size = 170;
            var width = img_two.width;
            var height = img_two.height;

            if (width > height)
            {
                if (width > max_size) 
                {
                    height *= max_size / width;
                    width = max_size;
                }
            }
            else
            {
                if (height > max_size)
                {
                    width *= max_size / height;
                    height = max_size;
                }
            }    

            canvas_two.width = width;
            canvas_two.height = height;
            ctx.drawImage(img_two, 0, 0, width, height);

            var imageData_two = ctx.getImageData(0,0,canvas_two.width,canvas_two.height);
            const pixel_data = imageData_two.data;
        
            for (var i = 0; i <= pixel_data.length; i += 4)
            {
               //Convert RGB values from img to 8-bit Binary 
               store_hidden_msb_bits(pixel_data[i], i);
               store_hidden_msb_bits(pixel_data[i+1], (i+1) );
               store_hidden_msb_bits(pixel_data[i+2], (i+2) );
            }
                        
            ctx.putImageData(imageData_two, 0, 0);
    }
             img_two.src = reader_two.result;
}
            reader_two.readAsDataURL(hiddenImage_upload.files[0]); //File reader reads uploaded file from input
});

function encrypt_image(e)
 { 
      canvas.parentNode.removeChild(canvas);  //Removes Prior document Elements
      canvas_two.remove();       
      coverImage_caption.remove(); 
      hiddenImage_caption.remove(); 
      coverImage_upload.remove(); 
      hiddenImage_upload.remove(); 
    // //altered_image_caption.setAttribute("id", "altered_image_caption"); // Add ID to variable


    canvas_three.addEventListener('mousemove', alter_pixel_display);
    canvas_three.addEventListener("click", download, false);
    canvas_three.style.visibility = "visible";
    encryption();
}



function encryption() //Displays Image on Canvas 3
{
    const reader = new FileReader();
    reader.onload = function () 
    {
        const img = new Image()
        img.onload = function() 
        {    
           // Resizes the canvas to match that of the image
            var max_size = 170;
            var width = img.width;
            var height = img.height;

            if (width > height)
            {
                if (width > max_size) 
                {
                    height *= max_size / width;
                    width = max_size;
                }
            }
            else
            {
                if (height > max_size)
                {
                    width *= max_size / height;
                    height = max_size;
                }
            }    
    
            canvas_three.width = width;
            canvas_three.height = height;
            canv_three_ctx.drawImage(img, 0, 0, width, height);

            imageData = canv_three_ctx.getImageData(0,0,canvas_three.width,canvas_three.height);
            const pixel_data = imageData.data;       
            var pointer = 0;
            
            for (var i = 0; i <= pixel_data.length; i += 4)
            {

               pointer = i;
 
                var alt_r = new_pixel(pixel_data[i], pointer); 
                store_newImg(alt_r, pointer);    
                pointer +=1;      
                
                var alt_g = new_pixel(pixel_data[i+1], pointer);
                store_newImg(alt_g, pointer);
                pointer +=1;
                
                var alt_b = new_pixel(pixel_data[i+2], pointer);
                store_newImg(alt_b, pointer);

              pixel_data[i + 1] = bin_to_dec(alt_g);
              pixel_data[i + 2] = bin_to_dec(alt_b);

                if(i == 9252)
                {
                    console.log("Final Pixel Value (R): " + new_img_arr[i]    +  "| Original Image: " + cover_img_arr[i]     +  "| Hidden Image: " + hidden_img_pix[i]    +  "| Posit: " + i);
                    console.log("Final Pixel Value (G): " + new_img_arr[i+1]  +  "| Original Image: " + cover_img_arr[i+1]   +  "| Hidden Image: " + hidden_img_pix[i+1]  +  "| Posit: " + (i+1));
                    console.log("Final Pixel Value (B): " + new_img_arr[i+2]  +  "| Original Image: " + cover_img_arr[i+2]   +  "| Hidden Image: " + hidden_img_pix[i+2]  +  "| Posit: " + (i+2));        
                }

            }


            canv_three_ctx.putImageData(imageData, 0, 0);
            
                console.log("New Image Map: " + final_img_map);
                console.log("");
                console.log("Hidden Img Map: " + hidden_img_map);
                console.log("");
                console.log("Original Img Map: " + cover_img_map);
                console.log("");

            for (var i = 9252; i <= final_img_map.length; i += 4)
            {
                // console.log("Final Pixel Value (R): " + new_img_arr[i]    +  "| Original Image: " + cover_img_arr[i]     +  "| Hidden Image: " + hidden_img_pix[i]    +  "| Posit: " + i);
                // console.log("Final Pixel Value (G): " + new_img_arr[i+1]  +  "| Original Image: " + cover_img_arr[i+1]   +  "| Hidden Image: " + hidden_img_pix[i+1]  +  "| Posit: " + (i+1));
                // console.log("Final Pixel Value (B): " + new_img_arr[i+2]  +  "| Original Image: " + cover_img_arr[i+2]   +  "| Hidden Image: " + hidden_img_pix[i+2]  +  "| Posit: " + (i+2));  
            }

        }
             img.src = reader.result;
}
            reader.readAsDataURL(cover_img_input[0]); //File reader reads uploaded file from input
}



/*
RESULT:
2988:    sonic R: 00000100      Shadow R: 00        new Value R: 00000100
2989:    sonic G: "00010000"    Shadow G: 00       00010000
2990:    sonic B: 00010000      Shadow G: 00        00010000   
*/










function bin_to_dec(string)
 { 
    return parseInt((string + '').replace(/[^01]/gi, ''), 2);
 }

function store_hidden_msb_bits(val, pix_key)  //Convert pixel value to 8-Bit Binary (function)
{                                    // Inputs the value into array
    var output = "";
    var updated_pix = "";

    if(val != undefined)
    {
        updated_pix = ("00000000"+val.toString(2)).slice(-8);
        output = updated_pix.slice(0, 2); //retieves MSB pixel values
        hidden_img_pix.push(output);

         hidden_img_map[pix_key] = output;
    }
}

  function store_coverImg(pixel_val, key)
 {
    if(pixel_val != undefined)
    {
        var val = pixel_val.toString(2);
        var bin = ("00000000" + val).slice(-8); //Obtain cover 8-bit binary value
        cover_img_arr.push(bin);    //Store 8-bit binary value in array

        cover_img_map[key] = bin;
    }
 }

 function store_newImg(pixel_val, key_val)
 {
    var bin = pixel_val;
    final_img_map;
    new_img_arr.push(bin);

    final_img_map[key_val] = pixel_val;
 }

 function decimalToBinary(val)  //Convert pixel value to 8-Bit Binary (function)
 {
     if(val != undefined)
     {
         var output = ("00000000"+val.toString(2)).slice(-8);
         return output;
     }
     
     return 0; //Last Pixel gives undefined value, This identifies last pixel
 }


 function alter_pixel_display(event) //Display canvas pixel values (function)
 {
     var x = event.layerX;
     var y = event.layerY;
   
     var pixel = context.getImageData(x, y, 1, 1);
     var data = pixel.data;
     var rgba = 'rgba(' + data[0] + ', ' + data[1] +
     ', ' + data[2] + ', ' + (data[3] / 255) + ')';
  
     color.textContent = rgba;
     // Small canvas cube Color
    ctx_cube.fillStyle = rgba; 
    ctx_cube.fillRect(0, 0, canvas_cube.width, canvas_cube.height);
 }


 function alter_pixel_display_two(event) //Display canvas pixel values (function)
 {
     var x = event.layerX;
     var y = event.layerY;
   
     var pixel = ctx.getImageData(x, y, 1, 1);
     var data = pixel.data;
     var rgba = 'rgba(' + data[0] + ', ' + data[1] +
     ', ' + data[2] + ', ' + (data[3] / 255) + ')';
  
     color.textContent = rgba;
     // Small canvas cube Color
    ctx_cube.fillStyle = rgba; 
    ctx_cube.fillRect(0, 0, canvas_cube.width, canvas_cube.height);
 }

 function alter_pixel_display_three(event) //Display canvas pixel values (function)
 {
     var x = event.layerX;
     var y = event.layerY;
   
     var pixel = canv_three_ctx.getImageData(x, y, 1, 1);
     var data = pixel.data;
     var rgba = 'rgba(' + data[0] + ', ' + data[1] +
     ', ' + data[2] + ', ' + (data[3] / 255) + ')';
  
     color.textContent = rgba;
     // Small canvas cube Color
    ctx_cube.fillStyle = rgba; 
    ctx_cube.fillRect(0, 0, canvas_cube.width, canvas_cube.height);
 }

 function bin_to_dec(string)
 { 
    return parseInt((string + '').replace(/[^01]/gi, ''), 2);
}
  

function new_pixel(val, arr_pos)  //Convert pixel value to 8-Bit Binary (function)
 {
   var zeros = "000000"; 
   var new_val = "";
   var hidden_img_msb = hidden_img_pix[arr_pos];


    if(val != undefined)
     {
        var output = ("00000000"+val.toString(2)).slice(-8);

        if(arr_pos >= hidden_img_pix.length)
        {
            //Change Remaining Image Pixels last two bits to zero (No more values in hidden img array)
            var lsb = "00";
            new_val = output.slice(0,6);
            output = new_val + lsb;
            return output;
        }  
    

        if(arr_pos < hidden_img_pix.length) //Iterate through message string and update image pixels
        {
            new_val = output.slice(0,6);
            output = new_val + hidden_img_msb;
            return output;
        }
     }
     
     return "00000000"; 
 }


 function download(event)
 {
     var image = canvas_three.toDataURL("image/png", 1.0).replace("image/png", "image/octet-stream");
     var link = document.createElement('a');
     link.download = "Eye_Spy_IMG.png";
     link.href = image;
     link.click();
 }

document.getElementById("New_Image").style.visibility = "hidden";
canvas.addEventListener('mousemove', alter_pixel_display);
canvas_two.addEventListener('mousemove', alter_pixel_display_two);
canv_three_ctx.addEventListener('mousemove', alter_pixel_display_three);


//
