// Get all variables
var inputImage = document.getElementById('file');
var errorResult = document.getElementById('result');
var img = document.getElementById('uploaded_img');


// Add a change listener to the file input to inspect the uploaded file.
inputImage.addEventListener('change', function()
{
    var file = this.files[0];
    // Basic type checking.
    if (file.type.indexOf('image') < 0) 
    {
        errorResult.innerHTML = 'Invalid type, Please only submit an image file under 3 MBs!';
        return;
    }

    //if File already exists in local storage, Delete it
    if(localStorage.getItem("imgData") != null) 
    {
        localStorage.removeItem("imgData");
    }
 

    // Create a file reader
    var fReader = new FileReader();

    // Add complete behavior
    fReader.onload = function() 
    {
        // Show the uploaded image to banner.
        // img.src = fReader.result;
        displayImg(img);

        errorResult.innerHTML = ' ';
        
   
    };
    fReader.readAsDataURL(file);
 
});


function displayImg(img) 
{
    var canvas = document.getElementById('uploaded_img');
    var ctx = canvas.getContext('2d');
    
 // Resize the image
 var max_size = 544;// TODO : pull max size from a site config
 var width = img.width;
 var height = img.height;
 

     if (width > height)
    {
        if (width > max_size) 
        {
            height *= max_size / width;
            width = max_size;
        }
    }
    else
   {
    if (height > max_size)
     {
     width *= max_size / height;
     height = max_size;
     }
   }

   canvas.width = width;
   canvas.height = height;
   ctx.drawImage(img, 0, 0, width, height);
}






