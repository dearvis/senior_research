function validate() {
        var allowedExtensions = ['jpeg', 'jpg', 'png','bmp','pdf'];
        var fileExtension = document.getElementById('file').value.split('.').pop().toLowerCase();
        var validFile = false;

            for(var index in allowedExtensions) 
            {
                if(fileExtension === allowedExtensions[index]) //Uploaded Image matches extension
                {
                   validFile = true;
                   location.href = "popup_b.html";
                   break;
                }
               
            }

            if(!validFile) 
            {
                document.getElementById("statusMes").innerHTML = "Upload Failed !";
                document.getElementById("statusMesPartTwo").innerHTML = "Make sure the file type is either jpg, png, bmp or pdf"
            }
}

            