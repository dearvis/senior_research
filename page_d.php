<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>popup_b</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>

    <style>
           .box{
        	width:385px;
        	height:90px;
            }
            
            .blue{
	        background: #044b8d;
            }   

            img{            
            width: 90px;
            height: 90px;
            background:  #044b8d;
            }

            .modal-content {
                float: left;
                                }
            
            .text-content {
                display: table-cell;
            }

            html, body {
                font-family: 'Open Sans', Arial, Helvetica, sans-serif;
                font-size: 25px;
                margin: 0;
                min-height: 180px;
                width: 384px;
                
            }
      
            .modal-icons {
                padding-top: 10px;
                border-bottom: 35px solid  #044b8d;
                height: 50px;
                width: 100%;
            }
             
            .version{
                color: #444;
                font-size: 18px;
                
            }
            
            #Text{
                font-size: 12px;
                padding: 100px;
            }
            
            #custom-button {
        padding: 10px;
        color: white;
        background-color: #004594;
        border: 1px solid #000;
        cursor: pointer;
             }

    #custom-button:hover {
        background-color: #0b8eb6;
            }

    #custom-text {
        margin-left: 10px;
        font-family: sans-serif;
        color: #aaa;
        font-size: 12px
            }

            button.link_one{ 
                background:none;
                border:none; 
                font-size: 15px;
                text-align:center;
            }

            button.link_one:hover{ 
             color: #6085ec;
            }

            #uploadBttn {
        padding: 10px;
        color: white;
        background-color: #004594;
        border: 1px solid #000;
        cursor: pointer;
    }

    #uploadBttn:hover {
        background-color: #0b8eb6;
    }
            </style>
            
            <div class="box blue"> 
                    <div class="modal-header"
                    <h1 class="logo">
                     <img src="images/privateeye(125).png" alt="EyeSpy" class="logo-icon"> 
                    
                    </h1>
                </div>

            </div>
            
            </head>
            <body>
                       
            <div class="modal-content">
                    <img src=images/sample.png alt="sample" align="left">
                    <br>
                    <br>
  
            </div>

            <div class="text-content">      
            <div class="modal-content"  style="overflow: hidden; padding-right: .5em;">
                <form action="upload.php" method="POST" enctype="multipart/form-data" >
            <div style="overflow: hidden; padding-right: .5em;">
                <input type="file" style="width: 100%;" name="file" id="file" class="inputfile">
            </div>
            <div class="inner"></div>
                <br>
                <button type="submit" name="submit" class="uploadbuttonstyle" id="uploadBttn">Hide Data</button>
                </div>
    </form>
</div>

            <div class="modal-icons">
               
               </div>
            
               <script src="script_routing.js"></script>
            </body>
            </html>