<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>popup_b</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>

    <style>
        .column {
  float: left;
  width: 25%;
  padding: 5px;
}

/* Clear floats after image containers */
.row::after {
  content: "";
  clear: both;
  display: table;
}
           .box{
        	width:385px;
        	height:90px;
            }
            
            #caption1{
                font-size: 9px;
            }

            .blue{
            /*Top Colored Bar*/
	        background: #044b8d;
            }   

            img{            
            width: 90px;
            height: 90px;
            background:  #f5f7f8;
            }

            .modal-content {
                float: left;
            }
            
            .text-content {
               font-size: 15px;
            }

            html, body {
                font-family: 'Open Sans', Arial, Helvetica, sans-serif;
                font-size: 25px;
                margin: 0;
                min-height: 180px;
                width: 384px;
                
            }
      
            .bottom-header {
                /*Bottom Colored Bar*/
                font-size: 12px;
                border-top: 35px solid  #044b8d;
                height: 10px;
                width: 100%;
            }
             

             .social_logos{
                padding-left: 85px;
             }


            .version{
                color: #444;
                font-size: 18px;
            }
            
            .bottom-text{
                font-size: 15px;
                padding-left: 35px;
            }

            button.link_one{ 
                background:none;
                border:none; 
                font-size: 15px;
            }

            button.link_two{ 
                background:none;
                border:none; 
                font-size: 15px;
            }

            button.link_one:hover{ 
             color: #6085ec;
            }

            button.link_two:hover{ 
                color: #6085ec;
            }
            </style>
            
            <div class="box blue"> 
                    <div class="modal-header"
                    <h1 class="logo">
                     <img src="images/privateeye(125).png" alt="EyeSpy" class="logo-icon"> 
                    
                    </h1>
                </div>

            </div>
            
            </head>
            <body>
                       
            <div class="modal-content">
                    <div class="row">
                        
                            <div class="column">
                                    <img src=images/sample.png alt="sample">
                                    <figcaption id="caption1"> Cover Image </figcaption>
                            </div>
                            <div class="column">
                            <div class="text-content">      
                                <button onclick="" class="link_one" id="buttonOne">Download Cover Image</button>   
                                <br>
                                <br>
                                <button onclick="clicked_five()" class="link_two" id="buttonTwo">Done</button>
                                <br>
                                <br> 
                                        </div>
                            </div>
            </div>

           

            <div class="bottom-header">
               
            </div>
            
            <div class="bottom-text">
            <p>Post and showcase your embedded Image!!!</p>
            </div>


            <div class="social_logos">
                    <div class="row">
                            <div class="column">
                             <a href="https://www.facebook.com/"><img src=images/facebook.png alt="facebook" style="width:30px;height:30px;"></a>
                            </div>
                            <div class="column">
                             <a href="https://www.instagram.com/"><img src=images/instagram.png alt="instagram" style="width:30px;height:30px;"></a>
                            </div>
                            <div class="column">
                              <a href="https://www.twitter.com/"><img src=images/twitter.png alt="twitter" style="width:30px;height:30px;"></a>
                            </div>      
            </div>
            
               <script src="script_routing.js"></script>
            </body>
            </html>