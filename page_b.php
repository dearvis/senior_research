<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>popup_b</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>

    <style>
           .box{
        	width:385px;
        	height:90px;
            }
            
            .column {
            float: left;
            width: 45%;
            padding: 5px;
            }

            /* Clear floats after image containers */
            .row::after {
            content: "";
            clear: both;
            display: table;
        }

            .blue{
	        background: #044b8d;
            }   

            img{            
            width: 90px;
            height: 90px;
            background:  #044b8d;
            }

            .modal-content {
                float: left;
            }
            
            .text-content {
                font-size: 15px;
            }

            html, body {
                font-family: 'Open Sans', Arial, Helvetica, sans-serif;
                font-size: 25px;
                margin: 0;
                min-height: 180px;
                width: 384px;
                
            }
      
            .modal-icons {
                border-top: 35px solid  #044b8d;
                height: 10px;
                width: 100%;
            }
             
            .version{
                color: #444;
                font-size: 18px;
            }
            
            button.link_one{ 
                background:none;
                border:none; 
                font-size: 15px;
            }

            button.link_two{ 
                background:none;
                border:none; 
                font-size: 15px;
            }

            button.link_one:hover{ 
             color: #6085ec;
            }

            button.link_two:hover{ 
                color: #6085ec;
            }



            </style>
            
            <div class="box blue"> 
                    <div class="modal-header"
                    <h1 class="logo">
                     <img src="images/privateeye(125).png" alt="EyeSpy" class="logo-icon"> 
                    
                    </h1>
                </div>

            </div>
            
            </head>
            <body>
                       
            <div class="modal-content">
                    <div class="row">
                        




                    <div class="column">
                        <?php
                        // Include the database configuration file
                        include 'dbConfig.php';
                        
                        // Get images from the database
                        $query = $db->query("SELECT * FROM images ORDER BY uploaded_date DESC");
                        
                        if($query->num_rows > 0){
                            while($row = $query->fetch_assoc()){
                                $imageURL = 'uploads/'.$row["file_name"];
                        ?>
                            <img src="<?php echo $imageURL; ?>" alt="" />
                        <?php }
                        }else{ ?>
                            <p>No image(s) found...</p>
                        <?php } ?>                         
                            </div>
                          
                            <div class="column">
                            <div class="text-content">   
                                    <button onclick="clicked_one()" class="link_one" id="buttonOne">Add Hidden Message</button>   
                                    <br>
                                    <br>
                                    <button onclick="clicked_two()" class="link_two" id="buttonTwo">Add Hidden Image</button>
                                    <br>
                                    <br>
                                        </div>
                            </div>
  
            </div>

            <div class="modal-icons">
               
               </div>
            
               <script src="script_routing.js"></script>
            </body>
            </html>